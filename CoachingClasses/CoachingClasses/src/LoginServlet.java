import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		try
		{
			String type=request.getParameter("type");
			String u=request.getParameter("uname");
			String p=request.getParameter("password");
			String q;
			
			Class.forName("com.mysql.jdbc.Driver");
			
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Project","root","root");
			
			if(type.equals("Student"))
			{
				 q="select * from student where email=(?) and password=md5('"+p+"')"; 
			}
			else
			{
				 q="select * from Admin where uname=(?) and password=md5('"+p+"')";
			}
			
			PreparedStatement st=con.prepareStatement(q);
			st.setString(1, u);
			
			ResultSet rs=st.executeQuery();
			
			if(rs.next())
			{
				HttpSession session=request.getSession();  
			     session.setAttribute("uname",u); 
				if(type.equals("Student"))
				{
					RequestDispatcher rd=request.getRequestDispatcher("/Home.html");
					rd.forward(request, response);
					
				}
				else
				{
					RequestDispatcher rd=request.getRequestDispatcher("/AdminHome.html");
					rd.forward(request, response);
				} 
			}
			else
			{
				out.println("Invalid user!");
				RequestDispatcher rd=request.getRequestDispatcher("/Login.html");
				rd.include(request, response);
			}
		}
		catch(Exception e)
		{
			out.println("Error!"+e);
		}
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
