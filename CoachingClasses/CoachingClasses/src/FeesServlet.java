

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Servlet implementation class FeesServlet
 */
@WebServlet("/FeesServlet")
public class FeesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FeesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		HttpSession session=request.getSession(false);  
        String ses=(String)session.getAttribute("uname");  
       
        String amount=request.getParameter("amt");
        String n="name";
        int t=0,c;
        
        try {
        		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");  
        		LocalDateTime now = LocalDateTime.now();  
        		String d=dtf.format(now);
        		
        		Class.forName("com.mysql.jdbc.Driver");
			
				Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Project","root","root");
				
				PreparedStatement pst=con.prepareStatement("select name,class from student where email=(?)");
				pst.setString(1, ses);
				
				ResultSet rs=pst.executeQuery();
				while(rs.next())
				{
					n=rs.getString(1);
					c=rs.getInt(2);
					if(c==10)
					{
						t=10000;
					}
					else if(c==9)
					{
						t=9000;
					}
					else if(c==8)
					{
						t=8000;
					}
					else if(c==7)
					{
						t=7000;
					}
					else
					{
						t=6000;
					}
				}
				
				PreparedStatement st=con.prepareStatement("insert into fees(uname,fees,date,total_fees) value(?,?,?,?)");
				st.setString(1, n);
				st.setString(2, amount);
				st.setString(3, d);
				st.setInt(4, t);
				
				int i=st.executeUpdate();
				
				if(i>0)
				{
					//out.print("Paid");
					RequestDispatcher rd=request.getRequestDispatcher("/Mail");
					rd.forward(request,response);
				}
				else
				{
					//out.print("Not paid");
					RequestDispatcher rd=request.getRequestDispatcher("/Fees.html");
					rd.include(request, response);
				}
        }
        catch(Exception e)
        {
        	out.println("Exception:"+e);
        }
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
