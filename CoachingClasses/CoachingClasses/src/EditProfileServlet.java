

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class EditProfileServlet
 */
@WebServlet("/EditProfileServlet")
public class EditProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditProfileServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		HttpSession session=request.getSession(false);  
        String ses=(String)session.getAttribute("uname");  
        //out.println("ses");
        
        String n=request.getParameter("name");
		String a=request.getParameter("address");
		String d=request.getParameter("dob");
		String p=request.getParameter("pnno");
		String c=request.getParameter("class");
		String b=request.getParameter("board");
		
		try
		{
			/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			java.util.Date date =  sdf.parse(d); */
			
			Class.forName("com.mysql.jdbc.Driver");
			
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Project","root","root");
			
			PreparedStatement st=con.prepareStatement("update student set name=(?),address=(?),dob=(?),phone_no=(?),class=(?),board=(?) where email=(?)");
			st.setString(1, n);
			st.setString(2, a);
			st.setString(3, d);
			st.setString(4, p);
			st.setString(5, c);
			st.setString(6, b);
			st.setString(7, ses);
			//st.setString(8, pass);
			
			int i=st.executeUpdate();
			if(i==1)
			{
				out.println("Update Successful");
			}
		}
		catch(Exception e)
		{
			out.println("Error!"+e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
