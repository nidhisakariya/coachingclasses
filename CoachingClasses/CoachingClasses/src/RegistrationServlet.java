

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;


/**
 * Servlet implementation class RegistrationServlet
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * 
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String n=request.getParameter("name");
		String a=request.getParameter("address");
		String d=request.getParameter("dob");
		String p=request.getParameter("pnno");
		String c=request.getParameter("class");
		String b=request.getParameter("board");
		String e=request.getParameter("email");
		String pa=request.getParameter("password");
		String rp=request.getParameter("rpassword");
		//String pass=hashWord(pa);
		
		if(pa.equals(rp)){}
		else
		{
			out.println("password doesn't match");
			RequestDispatcher rd=request.getRequestDispatcher("/Registration.html");
			rd.include(request, response);
		}
		
		try
		{
			/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			java.util.Date date =  sdf.parse(d); */
			
			Class.forName("com.mysql.jdbc.Driver");
			
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Project","root","root");
			
			PreparedStatement st=con.prepareStatement("insert into student(name,address,dob,phone_no,class,board,email,password,status) values(?,?,?,?,?,?,?,md5('"+pa+"'),'"+"a"+"')");
			st.setString(1, n);
			st.setString(2, a);
			st.setString(3, d);
			st.setString(4, p);
			st.setString(5, c);
			st.setString(6, b);
			st.setString(7, e);
			
			int i=st.executeUpdate();
			if(i==1)
			{
				RequestDispatcher rd=request.getRequestDispatcher("/Login.html");
				rd.forward(request, response);
			}
			con.close();
		}
		catch(Exception ex)
		{
			out.println("Error in writing!!"+ex);
		}
	}

	/*private Object hashWord(String pa) {
		// TODO Auto-generated method stub
		
	}*/

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
